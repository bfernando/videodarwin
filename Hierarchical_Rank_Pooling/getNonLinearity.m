%### Terms of Use
%--------------
% This Hierarchical Rank Pooling software is strictly for non-commercial academic use only. 
% This Hierarchical Rank Pooling code or any modified version of it may not be used for any commercial activity, such as:
%1. Commercial production development, Commercial production design, design validation or design assessment work.
%2. Commercial manufacturing engineering work
%3. Commercial research.
%4. Consulting work performed by academic students, faculty or academic account staff
%5. Training of commercial company employees.

%### License
%-------
% The analysis work performed with the program(s) must be non-proprietary work. 
% Licensee and its contract users must be or be affiliated with an academic facility. 
% Licensee may additionally permit individuals who are students at such academic facility 
% to access and use the program(s). Such students will be considered contract users of licensee. 
% The program(s) may not be used for commercial competitive analysis 
% (such as benchmarking) or for any commercial activity, including consulting.

% *************************************************************************
% Dependency : Lib Linear 
% Author : Basura Fernando (basura.fernando@anu.edu.au)
function Data = getNonLinearity(Data,nonLin)    
    switch nonLin
        case ''
            Data = rootExpandKernelMap(Data);
        case 'tanh'
            Data = tanh(Data);
        case 'ssr'
            Data = sign(Data).*sqrt(abs(Data));
        case 'chi2'
            Data = vl_homkermap(Data',2,'kchi2');
            Data = Data';
        case 'none'
            
        case 'chi2exp'
            u = vl_homkermap(Data',1,'kchi2')';	
            Data = rootExpandKernelMap(u);
        case 'ser'
            Data = rootExpandKernelMap(Data);    
            
    end
end
